package io.axen.beacon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Confi {
	
	public static String confiPath = "plugins/RainbowBeacon/confi.yml";

	public static void init() {
		File f = new File(confiPath);
	    if(!f.exists()) {
	        
	        FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
	        
	        ArrayList list = new ArrayList();
	        
	        fo.set("loop", 1);
	        fo.set("list", list);
	        /*fo.set("x", 0);
		    fo.set("y", 0);
		    fo.set("z", 0);
		    fo.set("world", "world");*/
		    
		    try {
				fo.save(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    }
	    else {
	    	
	    	FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
		    
	    	ArrayList list = new ArrayList();
	    	
	    	double bx;
	    	double by;
	    	double bz;
	    	
	    	World world;
	    	String words;
	    	
	    	int time;
	    	
	    	time = fo.getInt("loop");
	    	list = (ArrayList) fo.getList("list");
	    	
	    	fo.set("loop", time);
	    	fo.set("list", list);
	    	
	    	if(list.isEmpty()) {
	    	}
	    	else {
	    		int i = 0;
      		  Iterator<String> iter = list.iterator();
      		  while (iter.hasNext()) { 
      			  
      			  String name = iter.next();
	    			bx = (double) fo.getDouble(name + ".x");
	    			by = (double) fo.getDouble(name + ".y");
	    			bz = (double) fo.getDouble(name + ".z");
	    			words = fo.getString(name + ".world");
	    			
	    			
	    		    fo.set(name + ".x", bx);
	    		    fo.set(name + ".y", by);
	    		    fo.set(name + ".z", bz);
	    		    fo.set(name + ".world", words);
	    			
	    		    i++;
	    		}
	    		
	    	}
		    try {
				fo.save(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	
	    }
	}

	
	public static boolean confiAdd(Location loc) {
		
		File f = new File(Confi.confiPath);
		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
		
		int bx;
    	int by;
    	int bz;
    	World world;
    	String words;
    
    	world = loc.getWorld();
    	words = world.getName();
    	bx =(int) loc.getX();
    	by =(int) loc.getY();
    	bz =(int) loc.getZ();
    	
		ArrayList list = new ArrayList();
		list = (ArrayList) fo.getList("list");
		String name = (String) ""+bx+"/"+by+"/"+bz+"/"+words+"";
		
		if(!list.contains(name)) {
			
			
			list.add(name);
			
			
			fo.set("list", list);
			fo.set(name + ".x", bx);
			fo.set(name + ".y", by);
			fo.set(name + ".z", bz);
			fo.set(name + ".world", words);
			try {
				fo.save(f);
				return true;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}
		}
		else {

			return false;
		}
		
		
		
	}
	
	
	public static boolean confiRem(Location loc) {
		
		File f = new File(Confi.confiPath);
		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
		
		
		int bx;
    	int by;
    	int bz;
    	World world;
    	String words;
    	
    	bx =(int) loc.getX();
    	by =(int) loc.getY();
    	bz =(int) loc.getZ();
    	world = loc.getWorld();
    	words = world.getName();
		
			ArrayList list = new ArrayList();
			list = (ArrayList) fo.getList("list");
			String name = (String) ""+bx+"/"+by+"/"+bz+"/"+words+"";
			
		if(list.contains(name)) {
			
			Placing.placeBlock(100,name);
			
			list.remove(name);
			
			
			fo.set("list", list);
			fo.set(name, null);
			try {
				fo.save(f);
				return true;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}
			
			
		}
		else {
			return false;
		}
		
		
	}


	public static void confiList(Player p) {
		
		File f = new File(confiPath);
		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
	    
    	ArrayList list = new ArrayList();
    	
    	double bx;
    	double by;
    	double bz;
    	
    	World world;
    	String words;
    	
    	
    	list = (ArrayList) fo.getList("list");
    	
    	
    	if(list.isEmpty()) {
    		
    		p.sendMessage(Main.chatcolor + "§6Il n'y a aucun beacon enregistré");
    		
    	}
    	else {
    		int i = 0;
  		  Iterator<String> iter = list.iterator();
  		String coulour = "f";
  		  while (iter.hasNext()) { 
  			  if((i % 2) == 0) {
  				  coulour = "5";
  			  }
  			  else {
  				  coulour = "2";
  			  }
  			  
  			  String name = iter.next();
    			bx = (double) fo.getDouble(name + ".x");
    			by = (double) fo.getDouble(name + ".y");
    			bz = (double) fo.getDouble(name + ".z");
    			words = fo.getString(name + ".world");
    			
    			p.sendMessage("§"+coulour+"-----§r " + Main.chatcolor + "§"+coulour+"-----");
    			p.sendMessage("§"+coulour+" Monde : §7" + words);
    			p.sendMessage("§"+coulour+" X : §7" + bx);
    			p.sendMessage("§"+coulour+" Y : §7" + by);
    			p.sendMessage("§"+coulour+" Z : §7" + bz);
    			p.sendMessage("§"+coulour+"-------------------------");
    			
    			
    		    i++;
    		}
    		
    	}
		
	}

}


