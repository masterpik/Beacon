package io.axen.beacon;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.isOp()) {
				PlayerInventory inventory = p.getInventory();
				
				
				if (cmd.getName().equalsIgnoreCase("rbb")) {
					
					String arg = "null";
					
					if (args.length == 1) {
						arg = args[0];
					}
					else if (args.length == 0) {
						arg = "take";
					}
					else {
						arg = "null";
					}
					
					/*String arg = "null";
					
					if (args[0] == null) {
						arg = "take";
					}
					else {
						arg = args[0];
					}*/
					
					if (arg.equalsIgnoreCase("list")) {
						Confi.confiList(p);
						return true;
					}
					else if (arg.equalsIgnoreCase("take")){
						
						ItemStack item = new ItemStack(Material.BLAZE_ROD, 1);
						ItemMeta meta = (ItemMeta) item.getItemMeta();
						meta.setDisplayName(Main.chatcolor);
						item.setItemMeta(meta);
						inventory.addItem(item);
						p.sendMessage(Main.chatcolor + "§2Clique sur un beacon avec le baton jaune pour faire un arc en ciel");
						return true;
						
					}
					else {
						p.sendMessage(Main.chatcolor + "§2Tape /rbb pour avoir la magie en main");
						return true;
					}
					
					
					
					
				}
			}
			else {
				p.sendMessage(Main.chatcolor + "§4Tu doit etre Op pour avoir le pouvoir");
				return false;
			}
	        
	    } else {
	    	Bukkit.getLogger().info("Vous devez etre un joueur pour executer cette commande");
	    	return false;
	    }
		return false;
	}
	
}
