package io.axen.beacon;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Placing {
	
	public static void placing(Location loc, int choice, int type){
		//getLogger().info("Placing on");
		if(choice == 1) 
			loc.getBlock().setType(Material.GLASS);
		else if(choice == 2)
			loc.getBlock().setTypeIdAndData(95, (byte) type, false);
		else if(choice == 3)
			loc.getBlock().setType(Material.AIR);
	}
	
	public static void placeBlock(int nb, String name) {
		
		//getLogger().info("Place block");
		File f = new File(Confi.confiPath);
		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
		
  		
  		
		
		double bx;
    	double by;
    	double bz;
    	
    	World world;
    	String words;
    	
    	
    	
    	
	    bx = (double) fo.getDouble(name + ".x");
		by = (double) fo.getDouble(name + ".y");
		bz = (double) fo.getDouble(name + ".z");
		words = fo.getString(name + ".world");
		world = (World) Bukkit.getWorld(words);
		
		/*double bx = 0;
		double by = 20;
		double bz = 0;
		World world = Bukkit.getWorld("empty");*/
		
		Location loc1 = new Location(world, bx, by + 1, bz);
		Location loc2 = new Location(world, bx, by + 2, bz);
		Location loc3 = new Location(world, bx, by + 3, bz);
		Location loc4 = new Location(world, bx, by + 4, bz);
	
		
		
		if(nb == 1) {
			placing(loc1, 2, 14);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 2) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 14);
			placing(loc4, 2, 14);
		}
		else if (nb == 3) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 14);
			placing(loc4, 1, 0);
		}
		else if (nb == 4) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 1);
			placing(loc4, 2, 14);
		}
		else if (nb == 5) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 6) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 14);
			placing(loc4, 2, 1);
		}
		else if (nb == 7) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 1);
			placing(loc4, 1, 0);
		}
		else if (nb == 8) {
			placing(loc1, 2, 14);
			placing(loc2, 2, 1);
			placing(loc3, 2, 1);
			placing(loc4, 2, 1);
		}
		else if (nb == 9) {
			placing(loc1, 2, 1);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 10) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 1);
			placing(loc4, 2, 1);
		}
		else if (nb == 11) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 1);
			placing(loc4, 1, 0);
		}
		else if (nb == 12) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 4);
			placing(loc4, 2, 1);
		}
		else if (nb == 13) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 14) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 1);
			placing(loc4, 2, 4);
		}
		else if (nb == 15) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 4);
			placing(loc4, 1, 0);
		}
		else if (nb == 16) {
			placing(loc1, 2, 1);
			placing(loc2, 2, 4);
			placing(loc3, 2, 4);
			placing(loc4, 2, 4);
		}
		else if (nb == 17) {
			placing(loc1, 2, 4);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 18) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 4);
			placing(loc4, 2, 4);
		}
		else if (nb == 19) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 4);
			placing(loc4, 1, 0);
		}
		else if (nb == 20) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 5);
			placing(loc4, 2, 4);
		}
		else if (nb == 21) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 22) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 4);
			placing(loc4, 2, 5);
		}
		else if (nb == 23) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 5);
			placing(loc4, 1, 0);
		}
		else if (nb == 24) {
			placing(loc1, 2, 4);
			placing(loc2, 2, 5);
			placing(loc3, 2, 5);
			placing(loc4, 2, 5);
		}
		else if (nb == 25) {
			placing(loc1, 2, 5);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 26) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 5);
			placing(loc4, 2, 5);
		}
		else if (nb == 27) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 5);
			placing(loc4, 1, 0);
		}
		else if (nb == 28) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 11);
			placing(loc4, 2, 5);
		}
		else if (nb == 29) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 30) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 5);
			placing(loc4, 2, 11);
		}
		else if (nb == 31) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 11);
			placing(loc4, 1, 0);
		}
		else if (nb == 32) {
			placing(loc1, 2, 5);
			placing(loc2, 2, 11);
			placing(loc3, 2, 11);
			placing(loc4, 2, 11);
		}
		else if (nb == 33) {
			placing(loc1, 2, 11);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 34) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 11);
			placing(loc4, 2, 11);
		}
		else if (nb == 35) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 11);
			placing(loc4, 1, 0);
		}
		else if (nb == 36) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 10);
			placing(loc4, 2, 11);
		}
		else if (nb == 37) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 38) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 11);
			placing(loc4, 2, 10);
		}
		else if (nb == 39) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 10);
			placing(loc4, 1, 0);
		}
		else if (nb == 40) {
			placing(loc1, 2, 11);
			placing(loc2, 2, 10);
			placing(loc3, 2, 10);
			placing(loc4, 2, 10);
		}
		else if (nb == 41) {
			placing(loc1, 2, 10);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 42) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 10);
			placing(loc4, 2, 10);
		}
		else if (nb == 43) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 10);
			placing(loc4, 1, 0);
		}
		else if (nb == 44) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 14);
			placing(loc4, 2, 10);
		}
		else if (nb == 45) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		else if (nb == 46) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 10);
			placing(loc4, 2, 14);
		}
		else if (nb == 47) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 14);
			placing(loc4, 1, 0);
		}
		else if (nb == 48) {
			placing(loc1, 2, 10);
			placing(loc2, 2, 14);
			placing(loc3, 2, 14);
			placing(loc4, 2, 14);
		}
		else if (nb == 100){
			placing(loc1, 3, 0);
			placing(loc2, 3, 0);
			placing(loc3, 3, 0);
			placing(loc4, 3, 0);
		}
		else {
			placing(loc1, 1, 0);
			placing(loc2, 1, 0);
			placing(loc3, 1, 0);
			placing(loc4, 1, 0);
		}
		
		
	}
	

}
