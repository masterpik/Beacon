package io.axen.beacon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Event implements Listener {

	private String chatcolor = Main.chatcolor;
	
	@EventHandler
	public void click(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
    			
		if(a == Action.RIGHT_CLICK_BLOCK) {
			
			Block b = e.getClickedBlock();
			Location loc = b.getLocation();
		
			
			
	    	
				if(p.getInventory().getItemInHand().hasItemMeta() == true && p.getInventory().getItemInHand().getType() == Material.BLAZE_ROD && p.getInventory().getItemInHand().getItemMeta().getDisplayName().equals(chatcolor)) {
					if(p.isOp()) {
						e.setCancelled(true);
						
						if(b.getType() == Material.BEACON) {
							
							boolean si = false;
							si = Confi.confiAdd(loc);
							
							if(si) {
								p.sendMessage(chatcolor + "§2Nouveau beacon enregistré");
							}
							else {
								p.sendMessage(chatcolor + "§4Ce beacon est déjà enregistré");
							}
							
						}
						else {
							p.sendMessage(chatcolor + "§4Vous devez cliquer sur un beacon");
						}
					}
					else {
						p.sendMessage(chatcolor + "§4Tu doit etre Op pour avoir le pouvoir");
					}
				}
		}
		else if (a == Action.LEFT_CLICK_BLOCK) {
			
			Block b = e.getClickedBlock();
			
			Location loc = b.getLocation();
			

				if(p.getInventory().getItemInHand().hasItemMeta() == true && p.getInventory().getItemInHand().getType() == Material.BLAZE_ROD && p.getInventory().getItemInHand().getItemMeta().getDisplayName().equals(chatcolor)) {
					if(p.isOp()) {
						e.setCancelled(true);
						
						if(b.getType() == Material.BEACON) {
								
							boolean sis = false;
							sis = Confi.confiRem(loc);
							
							if(sis) {
								p.sendMessage(Main.chatcolor + "§2Beacon supprimé");
							}
							else {
								p.sendMessage(Main.chatcolor + "§4Ce beacon n'est pas enregistré");
							}
							
						}
						else {
							p.sendMessage(chatcolor + "§4Vous devez cliquer sur un beacon");
						}
					}
					else {
						p.sendMessage(chatcolor + "§4Tu doit etre Op pour avoir le pouvoir");
					}
				}
		}
			
	}
	
	
}
