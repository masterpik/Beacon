package io.axen.beacon;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	

	public static String chatcolor = "§8[§cRa§ein§abo§bwB§dea§6co§fn§8] §r";
	public static int loop = 1;
	public static int task;
	
	@Override	
	public void onEnable() {
		
		Listener listen = new Event();
		PluginManager pm = Bukkit.getServer().getPluginManager();
	    pm.registerEvents(listen, this);
	    
	    CommandExecutor cmdExec = new Commands();
	    getCommand("rbb").setExecutor(cmdExec);
		
		Confi.init();	
    	
		timer();
    	
		getLogger().info("Allumage");
	}
	
	@Override
	public void onDisable() {
		
		Bukkit.getServer().getScheduler().cancelAllTasks();
		getLogger().info("Extinction");
	}
	
	
	@SuppressWarnings("deprecation")
	public void timer()
      {
		
		//getLogger().info("Timer on");
		File f = new File(Confi.confiPath);
		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
		int time;
		time = fo.getInt("loop");
			
		
              task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
                     
                      @SuppressWarnings({ "rawtypes", "unchecked" })
					@Override
                      public void run() {
                    	  	
                    	  //getLogger().info("Run on");
                    	  File f = new File(Confi.confiPath);
                  		FileConfiguration fo = YamlConfiguration.loadConfiguration(f);
                  		ArrayList list = new ArrayList();
                  		
                  		list = (ArrayList) fo.getList("list");
                    	  
                    	  if(!list.isEmpty()) {
                    		  
                    		  int i = 0;
                    		  Iterator<String> iter = list.iterator();
                    		  while (iter.hasNext()) { 
                    			  
                    			  String name = iter.next();
                    			  Placing.placeBlock(loop, name);
                    			  
                    			  i++;
                    		  }
                    		  
                    		  
                    	  	//int bucle = list.size();
          	    			//int boucle = 0;
          	    			//while(boucle < bucle) {
          	    				//String name = (String) list.get(boucle);
          	    			//	placeBlock(loop, name);
          	    			//}
                    	  	
	                    	
	                  		if(loop == 48) {
	                  			loop = 1;
	                  		}
	                  		else {
	                  			loop = loop + 1;
	                  		}
                    	  }
                      }
              }, 0, time);
      }
	
	
	  

}
